package DNDCore;

import java.awt.Graphics2D;
import java.awt.Point;


public abstract class CanvasObject implements Cloneable {

    private Point position;
    private Point translatory;
    private Object data;
    private Canvas canvas;
    private double widthScale;
    private double heightScale;
    private boolean ignoredSize;
    
    public CanvasObject(Point position)
    {
          this.position = position;
          
          widthScale = 1.0;
          heightScale = 1.0;
          
          translatory = new Point(0, 0);
          ignoredSize = false;
    }
    
    /**
     * Ajusta el factor de escala del ancho.
     * @param scale Factor de escala.
     */
    public void setWidthScale(double scale)
    {
        if(scale > 0)
            widthScale = scale;
    }
    
    /**
     * Ajusta el factor de escala de la altura.
     * @param scale Factor de escala
     */
    public void setHeightScale(double scale)
    {
        if(scale > 0)
            heightScale = scale;
    }
    
    /**
     * Consigue el factor de escala del ancho.
     * @return Factor de escala.
     */
    public double getWidthScale()
    {
        return widthScale;
    }
    
    /**
     * Consigue el factor de escala de la altura.
     * @return Factor de escala.
     */
    public double getHeightScale()
    {
        return heightScale;
    }
    
    /**
     * Mueve al CanvasObject a la posición "dest".
     * @param dest Nueva posición
     */
    public void move(Point dest)
    {
       position = dest;
    }
    
    /**
     * Consigue un punto que representa la posición (Esquina superior izquierd)
     * @return La posición.
     */
    public Point getPosition()
    {
          return position;
    }
    
    public void setPosition(Point position)
    {
        this.position = position;
    }
    
    /**
     * Ajusta un vector de traslación. Util cuando se utiliza
     * un Point compartido como posición y se necesita un ajuste.
     * @param t
     */
    public void setTranslatory(Point t)
    {
        translatory = t;
    }
    
    /**
     * 
     * @return Vector de traslación.
     */
    public Point getTranslatory()
    {
        return translatory;
    }
    
    /**
     * 
     * @return La posición efectiva en Canvas, es posición + traslación
     */
    public Point getEffectivePosition()
    {
        Point effectivePosition = new Point(position.x + translatory.x, position.y + translatory.y);
        
        return effectivePosition;
    }
    
    /**
     * Consigue una la referencia de una instancia asociada como dato.
     * @return La referencia como Object
     */
    public Object getData()
    {
        return data;
    }
    
    /**
     * Pasa la referencia de la instancia como dato.
     * @param data Referencia.
     */
    public void setData(Object data)
    {
        this.data = data;
    }
    
    /**
     * Ajusta canvas que contiene a este CanvasObject
     * @param canvas
     */
    public void setCanvas(Canvas canvas)
    {
        this.canvas = canvas;
    }
    
    /**
     * Consigue Canvas que contiene a este objecto.
     * @return Un Canvas.
     */
    public Canvas getCanvas()
    {
        return canvas;
    }
    
    /**
     * Ignorar a este objeto al determinar tamaño de Canvas.
     * @param value
     */
    public void setIgnored(boolean value)
    {
        ignoredSize = value;
    }
    
    public boolean getIgnored()
    {
        return ignoredSize;
    }
    
    abstract int getWidth();
    abstract int getHeight();
    abstract boolean contains(Point position);
    abstract void draw(Graphics2D engine);
    @Override
    public Object clone() throws CloneNotSupportedException
    {
            CanvasObject co = (CanvasObject)super.clone();
            co.setPosition(new Point(position.x, position.y));
            co.setTranslatory(new Point(translatory.x, translatory.y));

            return co;
    }
}
