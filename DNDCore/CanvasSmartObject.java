package DNDCore;


import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author DND Team
 */
public class CanvasSmartObject extends CanvasObject implements Cloneable {

    Canvas embeddedCanvas;
    Double[] scaleFactor;

    public CanvasSmartObject(Point position)
    {
        super(position);
        
        scaleFactor = new Double[2];
        scaleFactor[0] = new Double(getWidthScale());
        scaleFactor[1] = new Double(getHeightScale());
        
        embeddedCanvas = new Canvas(scaleFactor);
    }
    
    public void setScaleFactor(Double[] sFactor)
    {
        scaleFactor = sFactor;
    }
    
    @Override
    public void setCanvas(Canvas canvas)
    {
        super.setCanvas(canvas);
    }
    
    public void addElement(CanvasObject element)
    {
        embeddedCanvas.addElement(element);
    }

    public boolean removeElement(CanvasObject element)
    {
        return embeddedCanvas.removeElement(element);
    }
    
    public Canvas getEmbeddedCanvas()
    {
        return embeddedCanvas;
    }
    
    public void setEmbeddedCanvas(Canvas embedded)
    {
        embeddedCanvas = embedded;
    }
    
    
    private Point inversePoint(Point p)
    {
        p.x = -p.x;
        p.y = -p.y;
        return p;
    }
    
    public void draw(Graphics2D engine)
    {
        Point position = getPosition();
        Point newOrigin = new Point((int)(position.x/getWidthScale()), (int)(position.y/getHeightScale()));
        Canvas.changeOrigin(engine, newOrigin);
//        setPosition(newOrigin);
        embeddedCanvas.drawAll(engine);
//       setPosition(position);
        Canvas.changeOrigin(engine, inversePoint(newOrigin));
    }

    public CanvasObject containsObject(Point position) {
        Point p = position;
        Point op = getEffectivePosition();
        Point rp = new Point(p.x - op.x, p.y - op.y);
         
        /* rp : Relative position */
        for(CanvasObject o : embeddedCanvas.getElementsArray())
            if(o.contains(rp))
                return o;
        
        return null;
    }
    
    public boolean contains(Point position) 
    {
        if(containsObject(position) == null)
            return false;
        
        return true;
    }

    @Override
    int getWidth() {
        return embeddedCanvas.getSize().width;
    }

    @Override
    int getHeight() {
        return embeddedCanvas.getSize().height;
    }
    
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            CanvasSmartObject cso = (CanvasSmartObject) super.clone();
            Double[] nScaleFactor = new Double[2];
            nScaleFactor[0] = new Double(scaleFactor[0]);
            nScaleFactor[1] = new Double(scaleFactor[1]);
            cso.setScaleFactor(nScaleFactor);
            cso.setEmbeddedCanvas((Canvas)cso.getEmbeddedCanvas().clone());
            cso.getEmbeddedCanvas().setScaleFactor(nScaleFactor);
            return cso;
        }
        
   @Override
   public void setWidthScale(double factor)
   {
       super.setWidthScale(factor);
       scaleFactor[0] = new Double(getWidthScale());
   }
   
   @Override
   public void setHeightScale(double factor)
   {
       super.setHeightScale(factor);
       scaleFactor[1] = new Double(getHeightScale());
   }
   
   
}
