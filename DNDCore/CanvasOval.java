package DNDCore;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

/**
 * 
 * @author DND Team
 * @see CanvasObject
 */
public class CanvasOval extends CanvasRegion implements Cloneable {

    /**
     * Crea una figura ovalada de ancho "width" y altura "height".   
     * @param position Posición del CanvasOval
     * @param width Ancho del CanvasOval
     * @param height Alto del CanvasOval
     */
    public CanvasOval(Point position, int width, int height)
    {
        super(position, width, height);
    }
    
    /**
     * Dibuja figura ovalada.
     * @param engine Provee los metodos de dibujo.
     */
    public void draw(Graphics2D engine){
        engine.setColor(getBackground());
        
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);	
        engine.setRenderingHints(rh);
            
        engine.fillOval(getEffectivePosition().x, getEffectivePosition().y, getWidth(), getHeight());

    }

        @Override
        public Object clone() throws CloneNotSupportedException
        {
            return super.clone();
        }
}
