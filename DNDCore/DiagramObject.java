package DNDCore;

import DNDCore.CanvasSmartObject;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.swing.JMenuItem;

public abstract class DiagramObject implements Cloneable {

	private CanvasSmartObject drawing;
	Diagram diagram;
        ArrayList<Item> menuItems;
        CanvasLine line;
        
        public CanvasLine getLine()
        {
            return line;
        }
        
	public DiagramObject(CanvasSmartObject drawing)
	{
		this.drawing = drawing;
		drawing.setData((Object)this);
                menuItems = new ArrayList<Item>();
	}

        public Diagram getDiagram()
        {
            return diagram;
        }
        
        public void setDiagram(Diagram d)
        {
            diagram = d;
        }
        
	public int getX()
	{
		return drawing.getEffectivePosition().x;
	}

	public int getY()
	{
		return drawing.getEffectivePosition().y;
	}
        
	public void move(int x, int y)
	{
		Point d = new Point(x, y);

		drawing.move(d);
	}

	public CanvasSmartObject getDrawing()
	{
		return drawing;
	}

	public void setDrawing(CanvasSmartObject drawing)
	{
		this.drawing = drawing;
                
	}

	public JMenuItem[] getExtendedMenu() 
        {
            JMenuItem[] mi = new JMenuItem[menuItems.size()];
            for(int i = 0; i < menuItems.size(); i++) {
                Item item = menuItems.get(i);
                mi[i] = new JMenuItem(item.getTitle());
                mi[i].addActionListener(item.getActionListener());
            }
            
            return mi;
        }
        
        public void addMenuItem(Item item)
        {
               menuItems.add(item);
        }
        
        public ArrayList<Item> getMenuItems()
        {
            return menuItems;
        }
        
        public void removeMenuItem(Item item)
        {
            menuItems.remove(item);
        }
        
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            DiagramObject dObj = (DiagramObject) super.clone();
            CanvasSmartObject drawingClone = (CanvasSmartObject) dObj.getDrawing().clone();
            drawingClone.setCanvas(null);
            drawingClone.setData(dObj);
            dObj.setDrawing(drawingClone);
            
            /* Falta clonar la lista de menuItems? */
            
            return dObj;
        }
        
        public abstract void select();
        public abstract void unselect();

}
