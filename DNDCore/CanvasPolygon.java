package DNDCore;





import java.awt.Graphics2D;
import java.awt.Point;


public class CanvasPolygon extends CanvasRegion implements Cloneable {

    int polygonPointsx[], polygonPointsy[];
    int numPoints;
    
    public CanvasPolygon(int polygonPointsx[], int polygonPointsy[], Point position, int width, int height )
    {
        super(position, width, height);
        this.polygonPointsx = polygonPointsx;
        this.polygonPointsy = polygonPointsy;
        this.numPoints = polygonPointsx.length;
    }
    
    public void draw(Graphics2D engine){
        engine.setColor(getBackground());
        engine.fillPolygon(polygonPointsx, polygonPointsy, numPoints);
    }
    
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            return super.clone();
        }
}
