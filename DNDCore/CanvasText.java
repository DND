package DNDCore;


import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;


public class CanvasText extends CanvasRegion implements Cloneable {
    
    private String content;
    private Color color;
    private Font font;
    private int size;
    private String nameFont;
    private int fontType;
    private final int SIZE = 12;
        
    
    public CanvasText(Point position, String text)
    {
        super(position, 0, 0);
        this.content = text;
        setSize(SIZE);
        setFontType(Font.PLAIN);
        setNameFont("Sarif");
        setFont(new Font(getNameFont(), getFontType(), getSize())) ;

        setFontColor(Color.black);
    }
    
    public void setText(String text)
    {
        this.content = text;
    }
    
    public String getText()
    {
        return content;
    }
    
    public void setFont(Font font)
    {
        this.font = font;
    }
    
    public Font getFont()
    {
        return font;
    }
    
    public void setFontColor(Color color)
    {
        this.color = color;
    }
    
    public Color getFontColor()
    {
        return color;
    }
    
    public void setSize(int size)
    {
        this.size = size;
    }
    
    public int getSize()
    {
        return size;
    }
    
    public String getNameFont()
    {
        return nameFont;
    }
    
    public void setNameFont(String nameFont)
    {
        this.nameFont = nameFont;
    }
    
    public int getFontType()
    {
        return fontType;
    }
    
    public void setFontType(int fontType)
    {
        this.fontType = fontType;
    }


    public void draw(Graphics2D engine)
    {    
        engine.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        setFont(new Font(getNameFont(), getFontType(), getSize())) ;
        FontMetrics currentMetrics = engine.getFontMetrics(font);
        
        super.setWidth(currentMetrics.stringWidth(getText()));
        super.setHeight(currentMetrics.getHeight());
        
        engine.setFont(font);
        engine.setColor(getFontColor());

        engine.drawString(content, (int)(getPosition().getX()), (int)(getPosition().getY()+getHeight()));
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    
    
    private double getDrawedHeight() 
    {
        return getHeight() * getCanvas().getZoom();
    }
    
    private double getDrawedWidth()
    {
        return getWidth() * getCanvas().getZoom();
    }
    
}
