package DNDCore;





import java.awt.Graphics2D;
import java.awt.Point;


public class CanvasRectangle extends CanvasRegion implements Cloneable {
    
    public CanvasRectangle(Point position, int width, int height)
    {
        super(position, width, height);
    }
    
    public void draw(Graphics2D engine)
    {
        int width = getWidth();
        int height = getHeight();
        int x = getEffectivePosition().x;
        int y = getEffectivePosition().y;
        
        /* Soporte para rectangulos con width o height negativo:
         * Se translada la posición en la magnituPoint p = getPosition();d negativa y las dimensiones
         * se cambian a signo positivo. (Solo sirve para rectangulo de selección) 
         */
        if(width < 0) {
            x += width;
            width = -width;           
        }

        if(height < 0) {
            y += height;
            height = -height;
        }
        
        engine.setColor(getBackground());
        
        engine.fillRoundRect(x, y, width, height, 10, 10);
        
    }

        @Override
        public Object clone() throws CloneNotSupportedException
        {
            return super.clone();
        }
}
