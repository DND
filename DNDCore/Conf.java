package DNDCore;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 *
 * @author DND Team
 */
public class Conf {
    static Conf defaultConf = null;
    
    ArrayList<String> pluginURLs;
    ArrayList<String> pluginClasses;
    String confPath;
    
    public Conf(String confPath)
    {
        pluginURLs = new ArrayList<String>();
        pluginClasses = new ArrayList<String>();
        this.confPath = confPath;
    }
    
    static public Conf openConf(File XMLconf) throws FileNotFoundException, IOException
    {
        Conf conf;

       if (!XMLconf.exists()) {
            conf = new Conf(XMLconf.getAbsolutePath());
            saveConf(conf, XMLconf);
        } else
            conf = getConf(XMLconf);
        
        return conf;
    }

    static public void saveConf(Conf conf, File XMLconf) throws FileNotFoundException, IOException
    {
        XStream xstream = new XStream();
        
        XMLconf.createNewFile();
        OutputStream outputStream = new FileOutputStream(XMLconf);
        xstream.toXML(conf, outputStream);
        outputStream.close();
    }
    
    static public Conf getConf(File XMLconf) throws FileNotFoundException
    {
        Conf conf;
        XStream xstream = new XStream(new DomDriver());
        
        InputStream inputStream = new FileInputStream(XMLconf);
        conf = (Conf) xstream.fromXML(inputStream);
        
        return conf;
    }
    
    static public String getDNDPath()
    {
            String homePath = System.getProperty("user.home");
            String DNDPath = homePath + File.separator + "DND";
            return DNDPath;
    }
    
    public void save() throws FileNotFoundException, IOException
    {
        File XMLconf = new File(confPath);
        saveConf(this, XMLconf);
    }
    
    public void addPluginURL(String url)
    {
        pluginURLs.add(url);
    }
    
    public boolean removePluginURL(String url)
    {
        return pluginURLs.remove(url);
    }
    
    public ArrayList<String> getPluginURLs()
    {
        return pluginURLs;
    }

    public void addPluginClass(String className)
    {
        pluginClasses.add(className);
    }
    
    public boolean removePluginClass(String className)
    {
        return pluginClasses.remove(className);
    }
    
    public ArrayList<String> getPluginClasses()
    {
        return pluginClasses;
    }
    
    public static Conf getDefaultConf() throws FileNotFoundException, IOException 
    {
        if (defaultConf == null) {

            String DNDPath = Conf.getDNDPath();

            File dir = new File(DNDPath);

            if (!dir.exists()) {
                dir.mkdir();
            }

            File XMLconf = new File(DNDPath + File.separator + "DND.xml");

            defaultConf = Conf.openConf(XMLconf);
        }
        
        return defaultConf;
    }
}
