package DNDCore;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class Diagram {

        private Canvas canvas;
	private ArrayList<DiagramObject> elements;
        private Workspace workspace;

	public Diagram()
	{
		canvas = new Canvas(null);
		elements = new ArrayList<DiagramObject>();
	}

	public Canvas getCanvas()
	{
		return canvas;
	}

	public void addElement(DiagramObject object)
	{
		elements.add(object);
                canvas.addElement((CanvasObject)object.getDrawing());
                object.setDiagram(this);
	}

	public void removeElement(DiagramObject object)
	{
		CanvasObject drawing = object.getDrawing();
		canvas.removeElement(drawing);
		elements.remove(object);
	}
        
        public DiagramObject getElementIn(int x, int y)
        {
            CanvasObject canvasObject = canvas.getElementIn(x, y);
            
            if(canvasObject != null)
                return (DiagramObject) canvasObject.getData();
            
            return null;
        }

        public ArrayList<DiagramObject> getElementsIn(ArrayList<DiagramObject> list, int x, int y, int width, int height)
        {
            if(width < 0) {
                x += width;
                width = -width;
            }
            
            if(height < 0) {
                y += height;
                height = -height;
            }
            
            for(DiagramObject o : elements) {
                CanvasObject co = o.getDrawing();
                Point p = co.getEffectivePosition();
                if(x <= p.x && p.x+co.getWidth() <= x+width
                && y <= p.y && p.y+co.getHeight() <= y+height)
                    list.add(o);
            }
            
            return list;
        }
                
                
	public ArrayList<DiagramObject> getElements()
	{
		return elements;
	}
        
        public DiagramObject[] getElementsArr()
        {
            return elements.toArray(new DiagramObject[elements.size()]);
        }
        
        public static Diagram createDiagramFromFile(File diagramFile) throws FileNotFoundException 
        {
            XStream xstream = new XStream(new DomDriver());
            
            return (Diagram) xstream.fromXML(new FileInputStream(diagramFile));
        }
        
        public Workspace getWorkspace()
        {
            return workspace;
            
        }
        
        public void setWorkspace(Workspace w)
        {
            workspace = w;
        }
        
        public void saveToFile(File diagramFile) throws FileNotFoundException, IOException
        {
            XStream xstream = new XStream();
            diagramFile.createNewFile();
            Workspace w = workspace;
            workspace = null;
            OutputStream outputStream = new FileOutputStream(diagramFile);
            xstream.toXML(this, outputStream);
            outputStream.close();
            workspace = w;
        }
}