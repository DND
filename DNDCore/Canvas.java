package DNDCore;

import java.util.ArrayList;
import java.awt.*;

/**
 * Un canvas es en lo que uno puede dibujar. Nuestro canvas es un contenedor de CanvasObjects.
 * @see CanvasObject
 * Llama de manera ordenada el metodo "draw" de cada CanvasObject cuando necesita
 * redibujar todo.
 * @author DND Team
 */
public class Canvas implements Cloneable {

	private ArrayList<CanvasObject> elements;
        private double zoomFactor;
	private Double[] scaleFactor;
        private Color background;
        
        public static final Point origin = new Point(0, 0);

	public Canvas(Double[] sFactor)
	{
                background = Color.WHITE;
		elements = new ArrayList<CanvasObject>();
                if(sFactor == null) {
                    scaleFactor = new Double[2];
                    scaleFactor[0] = new Double(1.0);
                    scaleFactor[1] = scaleFactor[0];
                } else
                    setScaleFactor(sFactor);
                
                zoomFactor = 1.0;
	}

        public void setScaleFactor(Double[] sFactor) 
        {
            scaleFactor = sFactor;
        }
        
        public double getXScaleFactor()
        {
            return scaleFactor[0].doubleValue();
        }
        
        public double getYScaleFactor()
        {
            return scaleFactor[1].doubleValue();
        }
        
        public void setBackground(Color bg)
        {
            this.background = bg;
        }
        
        public Color getBackground()
        {
            return background;
        }
        
        /**
         * Agrega un canvasObject al final.
         * @param element CanvasObject a agregar.
         */
	public void addElement(CanvasObject element)
	{
		/* Nuevo CanvasObject se dibuja al ultimo; esta en una capa superior */
                System.out.println("Agregado canvas object");
		elements.add(element);
                element.setCanvas(this);
	}
        
        /**
         * Elimina a "element" del Canvas.
         * @param element Elemento a eleminar
         * @return Retorna true si esta "element" en Canvas
         */
	public boolean removeElement(CanvasObject element)
	{
		return elements.remove(element);
	}
	
        /**
         * 
         * @return Un array con los CanvasObject contenidos aquí.
         */
	public CanvasObject[] getElementsArray()
	{
		return (CanvasObject[])elements.toArray(new CanvasObject[elements.size()]);
	}
	
        public ArrayList<CanvasObject> getElements()
        {
            return elements;
        }
        
        public void setElements(ArrayList<CanvasObject> elements)
        {
            this.elements = elements;
        }
        
        /**
         * Ajusta un factor de zoom
         * @param factor Es un double que representa el escalado (1.0 es 100%)
         */
	public void setZoom(double factor)
	{
		zoomFactor = factor;
	}
	
        /**
         * 
         * @return Factor de zoom actual.
         */
	public double getZoom()
	{
		return zoomFactor;
	}
        
        /**
         * Busca un Canvas Object en el punto (x, y).
         * @param x Abscisa
         * @param y Ordenada
         * @return CanvasObject si hay uno en ese punto o null.
         */
	public CanvasObject getElementIn(int x, int y)
	{
		for(int i = elements.size() - 1; i >= 0; i--) {
			CanvasObject o = elements.get(i);
                        if(o.contains(new Point(x, y))) {
                            System.out.println("retornado");
                            return o;
                        }
		}
		return null;
	}

        
        /**
         * Cambia el punto de origen.
         * @param engine Provee metodos de dibujo.
         * @param newOrigin Punto nuevo origen
         */
	static public void changeOrigin(Graphics2D engine, Point newOrigin)
	{
		engine.translate(newOrigin.getX(), newOrigin.getY());
	}
	
        
        public Point toCanvasCoordinate(Point p)
        {
            Point ret = new Point();
            
            ret.x = (int)(p.x*(1.0/getZoom()));
            ret.y = (int)(p.y*(1.0/getZoom()));
            
            return ret;
        }
        
        public Dimension getSize()
        {
           int d;
           Dimension total = new Dimension(0, 0);

           for(CanvasObject o : elements) {
               if (!o.getIgnored()) {
                   d = (int) (o.getEffectivePosition().x * getZoom() + o.getWidth() * getZoom() * scaleFactor[0]);
                   if (total.width < d) {
                       total.width = d;
                   }

                   d = (int) (o.getEffectivePosition().y * getZoom() + o.getHeight() * getZoom() * scaleFactor[1]);
                   if (total.height < d) {
                       total.height = d;
                   }
               }
           }
           
           return total;
        }
        
        /**
         * Llama al metodo draw de cada CanvasObject desde el primero al último
         * la lista. Los últimos son los que quedan arriba.
         * @param engine
         * @param width
         * @param height
         */
	public void drawAll(Graphics2D engine)
        {
           /* Velocidad!! */
           RenderingHints rh = new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);	
           engine.setRenderingHints(rh);

           for(CanvasObject o : elements) {
               engine.scale(o.getWidthScale(), o.getHeightScale());
               o.draw(engine);
               engine.scale(1.0/o.getWidthScale(), 1.0/o.getHeightScale());
           }
	}
        
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            Canvas canvasClone = (Canvas)super.clone();
            CanvasObject coClone;
            canvasClone.setElements((ArrayList<CanvasObject>)canvasClone.getElements().clone());
            
            /* Arraylist's clone no clona los elementos, por lo tanto lo hago aca */
            for(CanvasObject co : canvasClone.getElementsArray()) {
                coClone = (CanvasObject)co.clone();
                canvasClone.removeElement(co);
                canvasClone.addElement(coClone);
            }
            
            return canvasClone;
        }
}
