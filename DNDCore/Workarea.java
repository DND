package DNDCore;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;


/**
 *
 * @author DND team
 */
public class Workarea extends JComponent {
    Workspace workspace;
    boolean requestShot;
    private boolean print;
    
    public Workarea(Workspace workspace) {
        this.workspace = workspace;
        setDoubleBuffered(true);
        setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
    }

    @Override
    public void paint(Graphics g) { 
        Graphics2D g2d;
        Canvas canvas = workspace.getDNDiagram().getCanvas();

        if(!requestShot) {
           g2d = (Graphics2D)g;
           g.setColor(canvas.getBackground());
           g.fillRect(0, 0, getWidth(), getHeight());
           g2d.scale(canvas.getZoom(), canvas.getZoom());
           canvas.drawAll(g2d);
        } else {
            
                requestShot = false;
                Dimension d = canvas.getSize();
                BufferedImage bImage = new BufferedImage(d.width, d.height, BufferedImage.TYPE_4BYTE_ABGR);
                g2d = bImage.createGraphics();
                g2d.setColor(canvas.getBackground());
                g2d.fillRect(0, 0, d.width, d.height);
                g2d.scale(canvas.getZoom(), canvas.getZoom());
                canvas.drawAll(g2d);
            try {
                File storedImage = new File(Conf.getDNDPath() + File.separator + "workarea.png");
                System.out.println(storedImage.getAbsolutePath());
                storedImage.createNewFile();

                ImageIO.write(bImage, "png", storedImage);
                repaint();
                            if(print==true){

                    print = false;
                    Image image = Toolkit.getDefaultToolkit().createImage(storedImage.getAbsolutePath());

                    Impresora imp = new Impresora();
                    imp.imprimir(image, this);

                }
            } catch (IOException ex) {
                Logger.getLogger(Workarea.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    public void setRequestShot(boolean value)
    {
        requestShot = value;
    }
    
    public void setPrint(boolean v)
    {
        print = v;
    }
}
