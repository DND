/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DNDCore;

import java.awt.event.ActionListener;

public class Item {
    private ActionListener actionListener;
    private String title;
    
    public Item(String title, ActionListener actionListener)
    {
        this.actionListener = actionListener;
        this.title = title;
    }
    
    public String getTitle()
    {
        return title;
    }
    
    public ActionListener getActionListener()
    {
        return actionListener;
    }
}
