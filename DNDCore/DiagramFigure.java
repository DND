package DNDCore;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

/**
 *
 * @author DND team
 */
public abstract class DiagramFigure extends DiagramObject implements Cloneable {
    private CanvasRectangle selection;
    private CanvasRectangle corner;
    public DiagramFigure(CanvasSmartObject drawing) {
        super(drawing);
        
        selection = new CanvasRectangle(new Point(-5, -5), 0, 0);
        selection.setBackground(new Color(0, 0, 200, 50));
        selection.setIgnored(true);
        corner = new CanvasRectangle(Canvas.origin, 0, 0);
        corner.setBackground(Color.ORANGE);
        corner.setIgnored(true);
    }
    
    public void select() 
    {
        CanvasSmartObject drawing = getDrawing();
        
        selection.setWidth((int)(drawing.getWidth()/drawing.getWidthScale()) + 8);
        selection.setHeight((int)(drawing.getHeight()/drawing.getHeightScale()) + 8);
        
        corner.setPosition(new Point(drawing.getWidth(), drawing.getHeight()));
        
        /* Mantiene el tamaño de este rectangulo aunque cambie el tamaño de la figura
         */
        corner.setWidthScale(1.0/drawing.getWidthScale());
        corner.setHeightScale(1.0/drawing.getHeightScale());
        
        corner.setWidth(6);
        corner.setHeight(6);
        
        drawing.addElement(selection);
        drawing.addElement(corner);
    }
    
    public CanvasRectangle getSelection()
    {
        return selection;
    }

    public void setSelection(CanvasRectangle cr)
    {
        selection = cr;
    }
    
    public void unselect()
    {
        getDrawing().removeElement(selection);
        getDrawing().removeElement(corner);
    }

    public void resize(int width, int height) {
        CanvasSmartObject cso = getDrawing();
        Canvas canvasEmbedded = cso.getEmbeddedCanvas();
        Dimension canvasSize = canvasEmbedded.getSize();

        float widthFactor = ((float)width) / canvasSize.width;
        float heightFactor = ((float)height) / canvasSize.height;

        cso.setWidthScale(cso.getWidthScale()*widthFactor);
        cso.setHeightScale(cso.getHeightScale()*heightFactor);
    }

    @Override
    public Object clone() throws CloneNotSupportedException 
    {
        return super.clone();
    }

}
