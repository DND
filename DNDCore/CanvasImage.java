package DNDCore;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;


public class CanvasImage extends CanvasRegion implements Cloneable {

    private Image image;
    Workarea w;
    public CanvasImage(Point position, Workarea w, Image img)
    {
        super(position,300,200);

        this.image = img;
       
       
    }
    
   public void setImage(Image image)
   {
        this.image = image;
   }
   
   public Image getImage()
   {
        return image;
   }
   
   public void draw(Graphics2D engine){

       engine.drawImage(image, getPosition().x, getPosition().y,getWidth(),getHeight(),w);
   }

   @Override
   public Object clone() throws CloneNotSupportedException
   {
        return super.clone();
   }
    
    private double getDrawedHeight() {
        return getHeight() * getCanvas().getZoom();
    }
    private double getDrawedWidth() {
        return getWidth() * getCanvas().getZoom();
    }
}
