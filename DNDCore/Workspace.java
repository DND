package DNDCore;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Workspace extends JScrollPane implements MouseListener, MouseMotionListener, ActionListener, KeyListener {

        private Workarea workarea;
	private Diagram diagram;
	private ArrayList<DiagramObject> selectedList;
        private Tool currentTool;
        private Rectangle visibleRectangle;
        private CanvasRectangle selectRectangle;
        private int startX, startY;
        private JPopupMenu dObjectPopupmenu;
        private JPopupMenu dPopupmenu;
        private JMenuItem cutItem;
        private JMenuItem copyItem;
        private JMenuItem pasteItem;
        private JMenuItem deleteItem;
        private ArrayList<DiagramObject> clipboard;
        private boolean clipboardCopy;
        private Tool oldTool;
        private DiagramFigure resizerObject;
        private DiagramObject inserterObject;
        private JMenuItem[] extendedMenu;

	public Workspace(Diagram diagram)
	{
		super(); 
                this.diagram = diagram;
                diagram.setWorkspace(this);
                setAutoscrolls(true);
                workarea = new Workarea(this);
                setViewportView(workarea);
                
		workarea.addMouseListener(this);
		workarea.addMouseMotionListener(this);
                
                setTool(Tool.SELECT);
                
                visibleRectangle = new Rectangle();
                selectedList = new ArrayList<DiagramObject>();
                // Menu para objeto seleccionado
                dObjectPopupmenu = new JPopupMenu();
                // Menu para diagrama
                dPopupmenu = new JPopupMenu();
                cutItem = new JMenuItem("Cortar");
                cutItem.addActionListener(this);
                dObjectPopupmenu.add(cutItem);
                copyItem = new JMenuItem("Copiar");
                copyItem.addActionListener(this);
                dObjectPopupmenu.add(copyItem);
                deleteItem = new JMenuItem("Eliminar");
                deleteItem.addActionListener(this);
                dObjectPopupmenu.add(deleteItem);
                pasteItem = new JMenuItem("Pegar");
                pasteItem.addActionListener(this);
                dPopupmenu.add(pasteItem);
                pasteItem.setEnabled(false);
                dObjectPopupmenu.add(new JSeparator());
                clipboard = new ArrayList<DiagramObject>();
                
                getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK), "pressed");
                getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK), "pressed");
                getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK), "pressed");
                getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK), "pressed");
                addKeyListener(this);
                
                extendedMenu = null;
        }

	public void setDNDiagram(Diagram diagram)
	{
		this.diagram = diagram;
	}

        public Diagram getDNDiagram()
        {
            return diagram;
        }

        public void addElement(DiagramObject element)
        {
            if(inserterObject != null) {

                
                inserterObject.unselect();
            }
            
            inserterObject = element;
            unselectAll();
            
            diagram.addElement(element);
            element.select();
            
            setTool(Tool.INSERTER);
        }
        
	public void updateWorkspace()
	{


            // No queremos que se redimensione con el rectangulo de selección
            if(selectRectangle == null) {
                Canvas canvas = diagram.getCanvas();
                
                int xMove = 0;
                int yMove = 0;
                ArrayList<DiagramObject> elements = diagram.getElements();
                for(DiagramObject o : elements)
                {
                    CanvasSmartObject cso = o.getDrawing();
                    Point p = cso.getPosition();
                    if(p.x < 0 && -p.x > xMove) {
                        xMove = -p.x;
                    }
                    
                    if(p.y < 0 && -p.y > yMove) {
                        yMove = -p.y;
                    }
                }
                
                for(DiagramObject o : elements) {
                    Point p = o.getDrawing().getPosition();
                    p.x += xMove;
                    p.y += yMove;
                }
                
                Dimension pd = canvas.getSize();
                int barWidth;
                
                barWidth = Math.max(15, getVerticalScrollBar().getWidth());
                pd.width = Math.max(pd.width, getSize().width - barWidth);
                barWidth = Math.max(15, getHorizontalScrollBar().getWidth());
                pd.height = Math.max(pd.height, getSize().height - barWidth);
            
                workarea.setPreferredSize(pd);
                workarea.setMinimumSize(pd);

                /* Creen que es correcto setear View cada vez que un componente
                 * cambia una propiedad de interes ? 
                 */
                setViewportView(workarea);
            }
            
            workarea.repaint();
	}

	public void mouseClicked(MouseEvent e) 
	{

	}

        public void mouseEntered(MouseEvent e) 
	{

        }

        public void mouseExited(MouseEvent e) 
        {

        }

        private void deleteSelected() {
             for (DiagramObject o : selectedList) {
                 if(o instanceof DiagramVertice)
                     ((DiagramVertice)o).disconnect();
                 
                 diagram.removeElement(o);
            }
        }

        private void unselectAll()
        {
            for(DiagramObject o : selectedList)
                o.unselect();
            
            selectedList.clear();
        }
        
        private void selectAll()
        {
            for(DiagramObject o : selectedList)
                o.select();
        }
        
    private void moveSelectedList(int xDiff, int yDiff) {
        for (DiagramObject o : selectedList) {
            Point curPos = o.getDrawing().getPosition();
            curPos.x += xDiff;
            curPos.y += yDiff;

        }
    }
        
        private void showMenu(JPopupMenu menu)
        {
            Point mousePosition = getViewport().getMousePosition();
            menu.show(this, mousePosition.x, mousePosition.y);

        }
        
    public void mousePressed(MouseEvent e) {
        DiagramObject diagramObject;
        Canvas canvas = diagram.getCanvas();

        requestFocus();
        
        switch (currentTool) {
            case SELECT:
                Point evPoint = canvas.toCanvasCoordinate(e.getPoint());
                startX = evPoint.x;
                startY = evPoint.y;
                if ((diagramObject = diagram.getElementIn(startX, startY)) != null) {
                    CanvasSmartObject drawing = diagramObject.getDrawing();
                    if (!selectedList.contains(diagramObject)) {

                        if (selectedList.size() > 0) {
                            unselectAll();
                        }

                        selectedList.add(diagramObject);
                        diagramObject.select();
                        // To top
                        diagram.removeElement(diagramObject);
                        diagram.addElement(diagramObject);
                        
                        updateWorkspace();
                    } else if(diagramObject instanceof DiagramFigure && e.getButton() == MouseEvent.BUTTON1){
                        // Es un objeto seleccionado
                        // Reviso si el cursor esta en la esquina inferior derecha.  
                        int cornerX = drawing.getPosition().x + drawing.getWidth();
                        int cornerY = drawing.getPosition().y + drawing.getHeight();
                        System.out.println(cornerX);
                        System.out.println(cornerY);
                        
                        System.out.println(evPoint);
                        if(evPoint.x >= cornerX-6 && evPoint.x <= cornerX + 6 && evPoint.y >= cornerY - 6 && evPoint.y <= cornerY + 6) {
                            setTool(Tool.RESIZER);
                            resizerObject = (DiagramFigure) diagramObject; 
                            resizerObject.unselect();
                            selectRectangle = new CanvasRectangle(new Point(drawing.getPosition().x, drawing.getPosition().y), drawing.getWidth(), drawing.getHeight());
                            selectRectangle.setBackground(new Color(0, 200, 0, 180));
                            canvas.addElement(selectRectangle);
                            updateWorkspace();
                        }
                    } 
                    
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        
                        if(extendedMenu != null)
                            for(JMenuItem mi : extendedMenu) {
                                dObjectPopupmenu.remove(mi);
                            }
                        
                        extendedMenu = diagramObject.getExtendedMenu();
                        if(extendedMenu != null) 
                            for(JMenuItem mi : extendedMenu) {
                                dObjectPopupmenu.add(mi);
                            }
                        
                        showMenu(dObjectPopupmenu);
                    }
                } else {
                    
                    if(selectedList.size() > 0)
                        unselectAll();
                    
                    if(selectRectangle != null)
                        canvas.removeElement(selectRectangle);
                        
                    if(e.getButton() == MouseEvent.BUTTON1) {
                        selectRectangle = new CanvasRectangle(new Point(startX, startY), 0, 0);
                        selectRectangle.setBackground(new Color(0, 255, 0, 50));
                        canvas.addElement(selectRectangle);
                    } else if(e.getButton() == MouseEvent.BUTTON3)
                        showMenu(dPopupmenu);

                }
                break;
            case HAND:
                startX = e.getPoint().x;
                startY = e.getPoint().y;
                break;
            case INSERTER:
                inserterObject.unselect();
                inserterObject = null;
                setTool(oldTool);
                break;
            }
    }

    public void mouseReleased(MouseEvent e) {
        Canvas canvas = diagram.getCanvas();

        switch (currentTool) {
            case SELECT:
                if (selectRectangle != null) {
                    canvas.removeElement(selectRectangle);
                    if(Math.abs(selectRectangle.getWidth() + selectRectangle.getHeight()) > 0)
                        diagram.getElementsIn(selectedList, selectRectangle.getEffectivePosition().x, selectRectangle.getEffectivePosition().y, selectRectangle.getWidth(), selectRectangle.getHeight());        

                    selectAll();
                    selectRectangle = null;
                }
                break;
            case HAND:
                break;
                
            case RESIZER:
                    canvas.removeElement(selectRectangle);
                    
                    int width = selectRectangle.getWidth();
                    int height = selectRectangle.getHeight();
                    
                    Point p = resizerObject.getDrawing().getPosition();

                    if(width < 0) {
                        p.x += width;
                        width = -width;
                    }
                    
                    if(height < 0) {
                        p.y += height;
                        height = -height;
                    }

                    resizerObject.resize(width, height);
                    
                    selectRectangle = null;                    
                    resizerObject.select();
                    resizerObject = null;
                    currentTool = oldTool;
                    break;
            }
        setTool(currentTool);
        updateWorkspace();
    }

	public void mouseDragged(MouseEvent e)
	{
            Canvas canvas = diagram.getCanvas();
            Point evPoint = canvas.toCanvasCoordinate(e.getPoint());
            
            switch(currentTool) {
         
                case SELECT:
                    if(selectedList.size() > 0) {
                        moveSelectedList(evPoint.x - startX, evPoint.y - startY);
                        workarea.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    } else if(selectRectangle != null) {
                        selectRectangle.setWidth(selectRectangle.getWidth() + evPoint.x - startX);
                        selectRectangle.setHeight(selectRectangle.getHeight() + evPoint.y - startY);
                    }
                     startX = evPoint.x;
                     startY = evPoint.y;
                    break;
                case HAND:
                    JViewport vp = getViewport();
                    Dimension visibleDimension = vp.getExtentSize();
                    Point mousePosition = vp.getMousePosition();
                    if(mousePosition != null) {
                        int xDistance = e.getPoint().x - startX;
                        int yDistance = e.getPoint().y - startY;

                        if(xDistance == 0) {
                            visibleRectangle.x = mousePosition.x;
                            visibleRectangle.width = 0;
                        } else
                        // Arrastre hacia la izquierda
                        if(xDistance < 0) {
                            visibleRectangle.x = visibleDimension.width;
                            visibleRectangle.width = -xDistance;
                        } else { // Arrastre hacia la derecha
                            visibleRectangle.x = -xDistance;
                            visibleRectangle.width = xDistance;
                        }
                        
                        if(yDistance == 0) {
                            visibleRectangle.y = mousePosition.y;
                            visibleRectangle.height = 0;
                        } else
                        // Arrastre hacia arriba
                        if(yDistance < 0) {
                            visibleRectangle.y = visibleDimension.height;
                            visibleRectangle.height = -yDistance;
                        } else { // Arrastre hacia abajo
                            visibleRectangle.y = -yDistance;
                            visibleRectangle.height = yDistance;
                        }
                        vp.scrollRectToVisible(visibleRectangle);
                    }
                    
                    break;
                case RESIZER:
                    int width = evPoint.x - resizerObject.getX();
                    int height = evPoint.y - resizerObject.getY();
                    selectRectangle.setWidth(width);
                    selectRectangle.setHeight(height);
                    break;
                    
            }
            updateWorkspace();

	}
        
	public void mouseMoved(MouseEvent e)
	{
            Canvas canvas = diagram.getCanvas();
            Point evPoint = canvas.toCanvasCoordinate(e.getPoint());
            
            switch(currentTool) {
                case INSERTER:
                    Point p = inserterObject.getDrawing().getPosition();
                    p.x = evPoint.x;
                    p.y = evPoint.y;
                    
                    updateWorkspace();
                    break;
            }
	}

    public void actionPerformed(ActionEvent e) {
        if (selectedList.size() > 0) {
            if (e.getSource() == cutItem) {
                clipboardCopy = false;
                clipboard.clear();
                pasteItem.setEnabled(true);
                clipboard.addAll(selectedList);
                deleteSelected();
                unselectAll();
                Point mousePosition = workarea.getMousePosition();
                for (DiagramObject o : clipboard) {
                    Point p = o.getDrawing().getPosition();
                    p.x -= mousePosition.x*(1.0/o.getDrawing().getCanvas().getZoom());
                    p.y -= mousePosition.y*(1.0/o.getDrawing().getCanvas().getZoom());
                }

                updateWorkspace();
            } else if (e.getSource() == copyItem) {
                clipboardCopy = true;
                clipboard.clear();
                pasteItem.setEnabled(true);
                Point mousePosition = workarea.getMousePosition();
                DiagramObject clonedO;
                for (DiagramObject o : selectedList) {
                    try {
                        o.unselect();
                        clonedO = (DiagramObject) o.clone();
                        o.select();
                        Point p = clonedO.getDrawing().getPosition();
                        p.x -= mousePosition.x * (1.0 / o.getDrawing().getCanvas().getZoom());
                        p.y -= mousePosition.y * (1.0 / o.getDrawing().getCanvas().getZoom());
                        clipboard.add(clonedO);
                    } catch (CloneNotSupportedException ex) {
                        Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("Copiado");
                    }

            
            } else if (e.getSource() == deleteItem) {
                deleteSelected();
                unselectAll();
                updateWorkspace();
            }
        }
            if (clipboard.size() > 0) {
                if (e.getSource() == pasteItem) {
                    unselectAll();
                    Point mousePosition = getViewport().getMousePosition();
                    selectedList.addAll(clipboard);
                    clipboard.clear();
                    if(clipboardCopy) {
                        for(DiagramObject o : selectedList) {
                        try {
                            clipboard.add((DiagramObject) o.clone());
                        } catch (CloneNotSupportedException ex) {
                            Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        }
                    } else
                        pasteItem.setEnabled(false);
                    
                    for (DiagramObject o : selectedList) {
                        diagram.addElement(o);
                        Point p = o.getDrawing().getPosition();
                        p.x += mousePosition.x*(1.0/o.getDrawing().getCanvas().getZoom());
                        p.y += mousePosition.y*(1.0/o.getDrawing().getCanvas().getZoom());
                    }
                    selectAll();
                    updateWorkspace();
                }
            }
        }
        
        public void setTool(Tool tool)
        {
            oldTool = currentTool;
            currentTool = tool;
            switch(tool) {
                case SELECT:
                    workarea.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                    break;
                case HAND:
                    if(selectedList.size() > 0) {
                        unselectAll();
                        updateWorkspace();
                    }
                    workarea.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    break;
            }
        }
        
        
        public Workarea getWorkarea()
        {
            return workarea;
        }
        
        public void setZoom(double factor)
        {
            Canvas canvas = diagram.getCanvas();
            canvas.setZoom(factor);
            
            updateWorkspace();
        }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        ActionEvent aEvent = null;
        if(e.getModifiers() == KeyEvent.CTRL_MASK) {
            switch(e.getKeyCode()) {
                case KeyEvent.VK_X:
                    aEvent = new ActionEvent(cutItem, 0, null, 0);
                    break;
                case KeyEvent.VK_V:
                    aEvent = new ActionEvent(pasteItem, 0, null, 0);
                    break;
                case KeyEvent.VK_C:
                    aEvent = new ActionEvent(copyItem, 0, null, 0);
                    break;
            }
            if(aEvent != null)
                actionPerformed(aEvent);
        }
    }

    public void keyReleased(KeyEvent arg0) {
        
    }

}

