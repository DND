package DNDCore;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;


public class CanvasLine extends CanvasObject implements Cloneable {
    
    private Point end;
    public final int NEAR = 6;
    private Color color;

    public CanvasLine(Point start, Point end)
    {
        super(start);
        this.end = end;
        color = Color.BLACK;
    }
    
    public void setEnd(Point end)
    {
        this.end = end;
    }
    
    public Point getStart()
    {
        return getPosition();
    }
    
    public Point getEffectiveEnd()
    {
        Point effectiveEnd = new Point(0, 0);
        
        effectiveEnd.x = end.x + getTranslatory().x;
        effectiveEnd.y = end.y + getTranslatory().y;
        
        return effectiveEnd;
    }
    
    public Point getEnd()
    {
        return end;
    }
    
    public void setColor(Color color)
    {
        this.color = color;
    }
    
    public Color getColor()
    {
        return color;
    }

    public void setTipo(int i) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setWidth(float parseFloat) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    void draw(Graphics2D engine) 
    { 
        engine.setPaint(color);
        engine.drawLine(getEffectivePosition().x, getEffectivePosition().y, getEffectiveEnd().x, getEffectiveEnd().y);
    }

    public boolean contains(Point position)
    {
       Point p[] = new Point[2];
       p[0] = getEffectivePosition();
       p[1] = getEffectiveEnd();
       
       int denom = p[1].x - p[0].x;
       int iMinorX = p[0].x < p[1].x ? 0 : 1;
       int iMinorY = p[0].y < p[1].y ? 0 : 1;
       int iMajorX = 1 - iMinorX;
       int iMajorY = 1 - iMinorY;

       if(denom == 0) {
           if(Math.abs(position.x - p[0].x) <= NEAR && position.y >= p[iMinorY].y && position.y <= p[iMajorY].y)
               return true;
           return false;
       } else // <diabolic>ha ha ha ha ha</diabolic> 
       if(position.x >= p[iMinorX].x && position.x <= p[iMajorX].x) {
           int num = p[1].y - p[0].y;
           float m = ((float)num)/denom;
           float y = m*position.x - m*p[0].x + p[0].y;
           if(Math.abs(y - position.y) <= NEAR)
               return true;
       }
       return false;
    
    }

    public int getWidth()
    {
        // No son necesarios
        return 0;
    }

    public int getHeight() 
    {
        // No son necesarios
        return 0;
    }
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            CanvasLine cl = (CanvasLine) super.clone();
            cl.setEnd(new Point(end.x, end.y));
            
            return cl;
        }
 }
