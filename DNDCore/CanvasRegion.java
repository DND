package DNDCore;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * Clase abstracta que representa una región rectangular. Todo CanvasObject
 * que pueda ser contenida en un rectangulo "imaginario" extendience esta clase.
 * @author DND Team
 */
public abstract class CanvasRegion extends CanvasObject implements Cloneable {

    private Color background;
    private int width;
    private int height;

    /**
     * @param position Posición del CanvasRegion.
     * @param width Ancho del CanvasRegion.
     * @param height Altura del CanvasRegion.
     */
    public CanvasRegion(Point position, int width, int height)
    {
        super(position);
        this.width = width;
        this.height = height;
        this.background = Color.BLACK;
    }
    

    /**
     * Ajusta ancho del CanvasRegion.
     * @param width Ancho.
     */
    public void setWidth(int width)
    {
        this.width = width;
    }
    
    /**
     * Ajusta altura del CanvasRegion.
     * @param height Altura.
     */
    public void setHeight(int height)
    {
        this.height = height;
    }
    
    /**
     * @return Ancho del CanvasRegion.
     */
    public int getWidth()
    {
        return width;
    }
    
    /**
     * @return Altura del CanvasRegion.
     */
    public int getHeight()
    {
        return height;
    }
    
    /**
     * @param bg Color de fondo de la figura.
     */
    public void setBackground(Color bg)
    {
        this.background = bg;
    }
    
    /**
     * @return Color de fondo de la figura
     */
    public Color getBackground()
    {
        return background;
    }

    /** 
     * @param position Punto por el que se pregunta.
     * @return true si la región contiene a ese punto.
     */
    public boolean contains(Point position)
    {
        int x = position.x;
        int y = position.y;
        int oX = getEffectivePosition().x;
        int oY = getEffectivePosition().y;
        Canvas canvas = getCanvas();
        
        if(x >= oX && x <= oX + getWidth()*getWidthScale()*canvas.getXScaleFactor() && y >= oY && y <= oY + getHeight()*getHeightScale()*canvas.getYScaleFactor())
            return true;
        
        return false;
    }

    public abstract void draw(Graphics2D engine);
    
        @Override
        public Object clone() throws CloneNotSupportedException
        {
            return super.clone();
        }
}
