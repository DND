package DNDCore;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;

public class CanvasTriangle extends CanvasRegion implements Cloneable {

    private Polygon triangle;
    
    public CanvasTriangle(Point position, int width, int height)
    {
        super(position, width, height);
        triangle = new Polygon();
        triangle.addPoint(0, 0);
        triangle.addPoint(0, 0);
        triangle.addPoint(0, 0);
        
    }
    
    public void draw(Graphics2D engine)
    {
        
        //          *
        //         / \
        triangle.xpoints[0] = getEffectivePosition().x + getWidth()/2;
        triangle.ypoints[0] = getEffectivePosition().y;
        //      .          
        triangle.xpoints[1] = getEffectivePosition().x;
        triangle.ypoints[1] = getEffectivePosition().y + getHeight();
        //       _______.
        triangle.xpoints[2] = getEffectivePosition().x + getWidth();
        triangle.ypoints[2] = triangle.ypoints[1];
        
        engine.setColor(getBackground());
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);	
        engine.setRenderingHints(rh);
            
        engine.fillPolygon(triangle);
        
    }

    @Override
        public Object clone() throws CloneNotSupportedException
        {
            return super.clone();
        }    
}