package DNDCore;

import DNDGui.EditorMain;
import com.thoughtworks.xstream.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DND implements Loader {
        Conf conf;
        EditorMain editor;
        
        public DND() throws FileNotFoundException, IOException
        {
            conf = Conf.getDefaultConf();
            editor = new EditorMain(this);
        }
        
        public void setVisible(boolean visible)
        {
            editor.setVisible(visible);
        }
}
