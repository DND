package DNDCore;

import DNDCore.CanvasLine;
import DNDCore.CanvasObject;
import DNDCore.CanvasOval;
import DNDCore.CanvasSmartObject;
import DNDCore.DiagramObject;
import java.awt.Color;
import java.awt.Point;
import javax.swing.JMenuItem;

/**
 *
 * @author DND Team
 */
public class DiagramVertice extends DiagramObject implements Cloneable {
    
    private DiagramVertice prev;
    private DiagramVertice next;
    private CanvasOval selectOval;
    private CanvasOval verticeOval;
    private CanvasLine line;
    private final Color selectColor = Color.RED;
    private final Color unselectColor;
    
    public DiagramVertice(Point position)
    {
        super(new CanvasSmartObject(position));
        unselectColor = new Color(0, 0, 0, 0);
        CanvasSmartObject so = getDrawing();
        selectOval = new CanvasOval(new Point(0, 0), 18, 18);
        selectOval.setBackground(unselectColor);
        verticeOval = new CanvasOval(new Point(6, 6), 6, 6);
        
        so.addElement(selectOval); /* 1º position */
        so.addElement(verticeOval); /* 2º position */
        line = null; /* Will be 3º position */
    }

    public static DiagramObject[] createComponent()
    {
        DiagramObject[] arr = new DiagramObject[3];
        
        DiagramVertice v0 = new DiagramVertice(new Point(50, 50));
        arr[0] = v0;
        
        DiagramVertice v1 = new DiagramVertice(new Point(100, 100));
        arr[1] = v1;
        
        v0.setNextVertice(v1);
        
        DiagramVertice v2 = new DiagramVertice(new Point(100, 50));
        arr[2] = v2;
        
        v1.setNextVertice(v2);
        
        return arr;
    }
    
    public void setNextVertice(DiagramVertice next)
    {
        this.next = next;
        
        if (next != null) {
            CanvasObject drawing = getDrawing();

            next.setPrevVertice(this);

            line = new CanvasLine(drawing.getPosition(), next.getDrawing().getPosition());
            line.setData(this);

            Point translatory = line.getTranslatory();
            translatory.move(9, 9);

            drawing.getCanvas().addElement(line);
        }
    }
    
    public void setPrevVertice(DiagramVertice prev)
    {
        this.prev = prev;
    }
    
    public DiagramVertice getPrevVertice()
    {
        return prev;
    }
    
    public DiagramVertice getNextVertice()
    {
        return next;
    }
    
    public CanvasOval getSelectOval()
    {
        return selectOval;
    }
    
    public void setSelectOval(CanvasOval so)
    {
        selectOval = so;
    }
    
    public CanvasOval getVerticeOval()
    {
        return verticeOval;
    }
    
    public void setVerticeOval(CanvasOval so)
    {
        verticeOval = so;
    }
    
    public void disconnect()
    {
        CanvasLine nextLine = null;
        CanvasLine prevLine = null;
        CanvasObject drawing = getDrawing();
        int t = 0;
        
        if(next != null) {
            nextLine = next.getLine();
            if(nextLine != null)
            if(nextLine.getPosition() == drawing.getPosition() || nextLine.getEnd() == drawing.getPosition())
                drawing.getCanvas().removeElement(nextLine);
        }
        
        if(prev != null) {
            prevLine = prev.getLine();
            prev.setNextVertice(next);
            
            if(prevLine != null)
            if(prevLine.getPosition() == drawing.getPosition() || 
                    nextLine.getEnd() == drawing.getPosition())
                drawing.getCanvas().removeElement(prevLine);
        }
        
        if(line != null)
            drawing.getCanvas().removeElement(line);
        
    }
    
    public CanvasLine getLine()
    {
        return line;
    }
    
    public void setLine(CanvasLine line)
    {
        this.line = line;
    }
    
    @Override
    public void select() {
        selectOval.setBackground(selectColor);
    }

    @Override
    public void unselect() {
        selectOval.setBackground(unselectColor);
    }

    @Override
    public DiagramObject clone() throws CloneNotSupportedException
    {
        DiagramVertice dv = (DiagramVertice)super.clone();
        CanvasObject clonedCanvasObjects[] = dv.getDrawing().embeddedCanvas.getElementsArray();
        dv.setLine((CanvasLine)clonedCanvasObjects[2]);
        dv.setSelectOval((CanvasOval)clonedCanvasObjects[0]);
        dv.setVerticeOval((CanvasOval)clonedCanvasObjects[1]);
        
        dv.setNextVertice((DiagramVertice)dv.getNextVertice().clone());
        dv.setPrevVertice((DiagramVertice)dv.getPrevVertice().clone());
        
        return dv;
    }
}
