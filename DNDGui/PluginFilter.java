/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DNDGui;
import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author felipe
 */
public class PluginFilter extends FileFilter {
   
   public boolean accept(File pathname)
   {
       if(pathname.isDirectory() || pathname.getName().endsWith(".dplug"))
           return true;
       
       return false;
   }

    public String getDescription() 
    {
        return "DND Plugin";
    }
   
   
}
