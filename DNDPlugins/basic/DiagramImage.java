/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DNDPlugins.basic;

import DNDCore.Canvas;
import DNDCore.CanvasImage;
import DNDCore.CanvasSmartObject;
import DNDCore.DiagramFigure;
import DNDCore.DiagramObject;
import DNDCore.Workarea;
import DNDCore.Workspace;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 * @author teck
 */
public class DiagramImage extends DiagramFigure implements ActionListener {
    private CanvasSmartObject so;
    static Image img;
    
    static {
        Toolkit tk = Toolkit.getDefaultToolkit();
        img = tk.createImage("/Data/icc.jpg");
    }

        public static DiagramObject[] createComponent()
        {
            DiagramObject[] arr = new DiagramObject[1];
            
            DiagramImage image = new DiagramImage(new Point(0, 0), null, img);
            arr[0] = image;
            return arr;
        }
         
    public DiagramImage(Point position, Workarea w, Image img){
        super(new CanvasSmartObject(position));
        so = getDrawing();
        so.addElement(new CanvasImage(new Point(0,0),w,img));
        
    }
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
