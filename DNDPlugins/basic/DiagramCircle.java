package DNDPlugins.basic;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import DNDCore.CanvasSmartObject;
import DNDCore.CanvasOval;
import DNDCore.Canvas;
import DNDCore.CanvasText;
import DNDCore.DiagramFigure;
import DNDCore.DiagramObject;
import DNDCore.Item;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JColorChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class DiagramCircle extends DiagramFigure implements ActionListener {

	private int anchorages[];

        public static DiagramObject[] createComponent()
        {
            DiagramObject[] arr = new DiagramCircle[1];
            DiagramCircle circle = new DiagramCircle(new Point(0, 0), 80);
            arr[0] = circle;
            return arr;
        }
        
	public DiagramCircle(Point position, int radius)
	{
           
            super(new CanvasSmartObject(position));
            CanvasSmartObject so;
            so = getDrawing();
            so.addElement(new CanvasOval(Canvas.origin, radius, radius));
            so.addElement(new CanvasText(new Point(0,radius/2), ""));
            
            
            Item menu1 = new Item("Crear Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                
                canvasText.setText(ingresarTexto());
                getDiagram().getWorkspace().updateWorkspace();
            }
            });
           addMenuItem(menu1); 

            
            Item menu2 = new Item("Modificar Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                
                canvasText.setText(ingresarTexto());
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            addMenuItem(menu2);
            
            Item menu3 = new Item("Eliminar Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                canvasText.setText("");
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            addMenuItem(menu3);
            
            
            Item menu4 = new Item("Cambiar color", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) 
            {
                Color newColor = JColorChooser.showDialog(null, "Tabla de colores", Color.BLUE);
                CanvasOval circle = (CanvasOval)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
                circle.setBackground(newColor);
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            this.addMenuItem(menu4);
            
	}
        
    public String ingresarTexto(){
        //jaime ve si este dialogo esta bien
         String newText = JOptionPane.showInputDialog("Ingrese nuevo texto");
         return newText;
    }
	public void actionPerformed(ActionEvent e)
	{

	}
}
