package DNDPlugins.basic;

import DNDCore.*;
import DNDCore.DiagramFigure;
import DNDCore.CanvasSmartObject;
import DNDCore.CanvasRectangle;
import DNDCore.Canvas;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DiagramText extends DiagramFigure implements ActionListener {

	private int anchorages[];
    
        private Component frame;
	public DiagramText(Point position, String text)
	{
            super(new CanvasSmartObject(position));
            CanvasSmartObject so;
            so = getDrawing();
            so.addElement(new CanvasText(Canvas.origin, text));
            
            
            Item menu1 = new Item("Tipo de Fuente", new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    modificarTipoFuente();
                    getDiagram().getWorkspace().updateWorkspace();
                }
            });
            this.addMenuItem(menu1);
            
            
            Item menu2 = new Item("Modificar Texto", new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    modificarTexto();
                    getDiagram().getWorkspace().updateWorkspace();
                }
            });
            this.addMenuItem(menu2);

            
            Item menu3 = new Item("Modificar Tamaño", new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    modificarTamanio();
                    getDiagram().getWorkspace().updateWorkspace();
                }
            });
            this.addMenuItem(menu3);

            
            Item menu4 = new Item("Modificar Color Fuente", new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    modificarColorFuente();
                    getDiagram().getWorkspace().updateWorkspace();
                }
            });
            this.addMenuItem(menu4);

            
            Item menu5 = new Item("Modificar Fuente", new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    modificarFuente();
                    getDiagram().getWorkspace().updateWorkspace();
                }
            });
            this.addMenuItem(menu5);

	}
    public void modificarFuente(){
        CanvasText canvasText = (CanvasText)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
        JOptionPane mensaje = new JOptionPane();
        String nombreFuente = "Arial";
        JTextField fuenteP = new JTextField();
        int tamanoFuente = 10;
        int x;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        //se trae un vector de objetos con los nombres de las fuentes del sistema
        Object opciones[] = ge.getAllFonts();
        String sub;
        String partes[];
        String[] fuentes;
        String cadenaFuentes = "";
        int cf = 1;

        //en este ciclo se extrae solo el nombre de la fuente
        for(x = 0; x < opciones.length; x ++){
            //en una sola linea se extrae justo el nombre
            partes = opciones[x].toString().substring(14, opciones[x].toString().length() - 1).split(",")[1].toString().split("=");
            if(partes.length > 1){
                cadenaFuentes += partes[1].toString() + ",";
            }
        }
        fuentes = cadenaFuentes.split(",");
        sub = (String)JOptionPane.showInputDialog(
                mensaje.getParent(),
                "Seleccione una fuente",
                "Fuentes del sistema",
                JOptionPane.PLAIN_MESSAGE,
                null,
                fuentes,
                fuentes[2]
         );

        canvasText.setNameFont(sub);
    }
    public void modificarColorFuente(){
        CanvasText canvasText = (CanvasText)getDrawing().getEmbeddedCanvas().getElementsArray()[0];

        Color initialColor = Color.white;

        JColorChooser chooser = new JColorChooser(initialColor);
        JFrame frame2 = new JFrame();
        JDialog dialog;
        boolean modal = false;
        Color newColor = JColorChooser.showDialog(frame, "Tabla de colores", initialColor);

        canvasText.setFontColor(newColor);
    
    }
    public void modificarTamanio(){
         String newSize = JOptionPane.showInputDialog("Ingrese nuevo tamaño");
        int x = Integer.valueOf(newSize);
        CanvasText canvasText = (CanvasText)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
        canvasText.setSize(x);

    }
    public void modificarTexto(){
        String newText = JOptionPane.showInputDialog("Ingrese nuevo texto");
         CanvasText canvasText = (CanvasText)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
         canvasText.setText(newText);
    }
    public void modificarTipoFuente(){
        CanvasText canvasText = (CanvasText)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
        Object[] options = {"Normal",
                    "Italic",
		    "Negrita"};
        int k = JOptionPane.showOptionDialog(frame,
            "Elija el tipo de letra ",
            "Tipo de letra",
            JOptionPane.YES_NO_CANCEL_OPTION,
           JOptionPane.QUESTION_MESSAGE,
         null,
         options,
         options[2]);
        //y estas son las codiciones usadas
        if(k==0){
            canvasText.setFontType(Font.PLAIN);
        }
        if(k==1){
            canvasText.setFontType(Font.ITALIC);
        }
        if(k==2){
            canvasText.setFontType(Font.BOLD);
        }
    }
	public void actionPerformed(ActionEvent e)
	{

	}
}
