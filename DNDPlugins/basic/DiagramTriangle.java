package DNDPlugins.basic;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import DNDCore.DiagramFigure;
import DNDCore.CanvasTriangle;
import DNDCore.CanvasSmartObject;
import DNDCore.Canvas;
import DNDCore.CanvasOval;
import DNDCore.CanvasRegion;
import DNDCore.CanvasText;
import DNDCore.DiagramObject;
import DNDCore.Item;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JColorChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class DiagramTriangle extends DiagramFigure implements ActionListener {

	private int anchorages[];

        
        public static DiagramObject[] createComponent()
        {
            DiagramObject[] arr = new DiagramObject[1];
            
            DiagramTriangle triangle = new DiagramTriangle(new Point(0, 0), 60, 60);
            arr[0] = triangle;
            
            return arr;
        }
	public DiagramTriangle(Point position, int width, int height)
	{
		super(new CanvasSmartObject(position));
                CanvasSmartObject so;
                so = getDrawing();
                so.addElement(new CanvasTriangle(Canvas.origin, width, height));
                so.addElement(new CanvasText(new Point(width/4,height/2),"asdf"));
                
                
            Item menu1 = new Item("Crear Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                
                canvasText.setText(ingresarTexto());
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            addMenuItem(menu1);

             
            Item menu2 = new Item("Modificar Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                
                canvasText.setText(ingresarTexto());
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            addMenuItem(menu2);
             
            Item menu3 = new Item("Eliminar Texto", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanvasText canvasText = (CanvasText) getDrawing().getEmbeddedCanvas().getElementsArray()[1];
                canvasText.setText("");
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            this.addMenuItem(menu3);

                        Item menu4 = new Item("Cambiar color", new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) 
            {
                Color newColor = JColorChooser.showDialog(null, "Tabla de colores", Color.BLUE);
                CanvasRegion reg = (CanvasRegion)getDrawing().getEmbeddedCanvas().getElementsArray()[0];
                reg.setBackground(newColor);
                getDiagram().getWorkspace().updateWorkspace();
            }
        });
            this.addMenuItem(menu4);
    }
    public String ingresarTexto(){
        //jaime ve si este dialogo esta bien
         String newText = JOptionPane.showInputDialog("Ingrese nuevo texto");
         return newText;
    }
	public void actionPerformed(ActionEvent e)
	{

	}
}
