import DNDCore.DND;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author DND Team
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        DND dnd = new DND();
        dnd.setVisible(true);
    }
    
}
